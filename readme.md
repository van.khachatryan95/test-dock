# Running the application
This guide will walk through the process of running the application in a development environment using docker, and docker-compose. This is only a getting started guide, and it is expected that the application will run under any production design docker orchestrator. While the configuration provided here is merely a sample one.

## Prerequisites
- Installed docker engine.
- Command shell with configured access to docker, and docker-compose.
- License key.

## Setup
Current directory contains:
- docker-compose.yaml file - sample docker configuration.
- config folder - sample account configuration files for EPS and HotelBeds.
- dumps folder - folder where request/response dumps will be stored
- postman-collections folder - sample requests as a postman collection with environment

Use the following command to login to Gimmonix container registry:
docker login -u gmx-client -p 8qkh8ig6Rb4Yl8AxKX9+m9CsRvAZB/TF gimmonix.azurecr.io

Output:

WARNING! Using --password via the CLI is insecure. Use --password-stdin.
Login Succeeded

## Execution
- Edit docker-compose.yaml and set your license key.
- From the current directory, run ‘docker-compose up’ to start the service.

## Usage
- The service is pre configured to run on port 8090 in your dev environment, and can be accessed using http://localhost:8090, you can change it by changing the docker-compose file.
- A swagger UI is available at http://{{baseurl}}/swagger, while the baseurl depends on your deployment method. With local docker, it’s going to be something like  http://localhost:8090 while the port might change.
- Open API spec http://{{baseurl}}/swagger/v2.0.34/swagger.json
- A health check endpoint available at  /health
- Test API using collections in postman-collections directory
    -  Import collection 'For Clients -  Dock Native API.postman_collection.json'
    -  Import environment 'Dock Native - Local.postman_environment.json'
    -  Select 'Dock Native - Local' environment in Postman
    -  Execute requests starting with search step

## Disclosure
This application is a proof of concept, and might not function as expected.
As such, we have allocated resources and we will make our best effort to resolve any blocking issues ASAP. While the application is in the POC stage, we are collecting logs of the application execution environment. This will not be the case in production.
Any feedback whether it's good, bad, or just an improvement request is more than welcomed.